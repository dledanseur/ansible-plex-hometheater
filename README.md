# Ansible playbook to install the plex home theater on ubuntu

Usage:

```
#!yaml

- host: my.host.example.com
  roles:
    - dledanseur.plex-hometheater
      vars:
        plex_hometheater_apt_repo: ppa:plexapp/plexht # optional
        plex_hometheater_apt_package: plexhometheater # optional
        plex_hometheater_autostart_user: auser # optional, defaults to none



```
